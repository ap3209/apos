# APOS

![App Screenshot](Screenshot/screenshot_2022-04-16_09-31-37.png)

![App Screenshot](Screenshot/screenshot_2022-04-16_09-32-25.png)

APOS is an Debian Linux post-installation script that will install and configer my rice of dwm window manager and associated programs that are needed to create a proper "desktop" experience.
## Installation instructions
```bash
git clone https://gitlab.com/ap3209/apos.git
cd apos/
chmod +x *.sh
sudo ./install.sh
```


#### `Note 1:` Key bindings for DWM window manager can be found in my DWM config file [here](https://gitlab.com/ap3209/dwm/-/blob/main/config.def.h#L83)

#### `Note 2:` Don't run this script on other Debian based distribution because of missing some packages on other distribution script may be failed.

#### `Note 3:` The script will install Flatpak but Flatpak apps are need to be install after restart of system.
```bash
cd apos/
./flatpak.sh
```

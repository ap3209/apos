#!/bin/sh

# flatpak apps
flatpak install https://flathub.org/repo/appstream/org.gimp.GIMP.flatpakref -y

flatpak install org.libreoffice.LibreOffice -y

flatpak install flathub org.telegram.desktop -y

flatpak install flathub org.kde.kdenlive -y

flatpak install flathub fr.handbrake.ghb -y

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo && flatpak install flathub net.mediaarea.MediaInfo -y

flatpak install flathub com.github.tchx84.Flatseal -y

flatpak install flathub com.obsproject.Studio -y

flatpak install flathub io.freetubeapp.FreeTube -y

#!/bin/bash

function brightness_type {
	arg=$1
	if [[ $arg = "up" ]]; then
		xbacklight -inc 10 && dunstify --icon="~/.window_manager_files_and_scripts/system_script/brightness.png" "Brightness Increase" -r 9993 "$(xbacklight -get|awk '{split($0,a,"."); print a[1]}')%" -t 2000
	elif [[ $arg = "down" ]]; then
		xbacklight -dec 10 && dunstify --icon="~/.window_manager_files_and_scripts/system_script/brightness.png" "Brightness Decrease" -r 9993 "$(xbacklight -get|awk '{split($0,a,"."); print a[1]}')%" -t 2000
	fi
}

brightness_type $1

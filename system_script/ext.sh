#!/bin/bash

function extract {
  if [ -f "$*" ] ; then
    case "$*" in
      *.tar.bz2)   tar xjfv "$*"   ;;
      *.tar.gz)    tar xzfv "$*"   ;;
      *.bz2)       bunzip2 -v "$*"   ;;
      *.rar)       unrar x "$*"   ;;
      *.gz)        gunzip -v "$*"    ;;
      *.tar)       tar xfv "$*"    ;;
      *.tbz2)      tar xjfv "$*"   ;;
      *.tgz)       tar xzfv "$*"   ;;
      *.zip)       unzip "$*"     ;;
      *.Z)         uncompress "$*";;
      *.7z)        7z x "$*"      ;;
      *.7z.001)	   7z x "$*"      ;;
      *.deb)       ar xv "$*"      ;;
      *.tar.xz)    tar xfv "$*"    ;;
      *.tar.zst)   tar --use-compress-program=unzstd -xvf "$*"    ;;
      *.ova)	   tar -xvf "$*"	   ;;	# Not type of regular zip file but act like zip file & used as Virtual Box bundle file.
      *)           echo "'$*' cannot be extracted via ext.sh" ;;
    esac
  else
    echo ""$*" is not a valid file"
  fi
}

extract $*

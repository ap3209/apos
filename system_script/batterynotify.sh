#!/bin/bash

export DISPLAY=:0
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus"
export XDG_RUNTIME_DIR="/run/user/1000"

var="$(wpctl get-volume @DEFAULT_AUDIO_SINK@|awk -F' ' 'END{print $2}')"

# Battery percentage at which to notify
WARNING_LEVEL=15
BATTERY_DISCHARGING="$(cat /sys/class/power_supply/BAT1/status | grep -c "Discharging")"
BATTERY_LEVEL="$(cat /sys/class/power_supply/BAT1/capacity)"

# Use two files to store whether we've shown a notification or not (to prevent multiple notifications)
EMPTY_FILE=/tmp/batteryempty
FULL_FILE=/tmp/batteryfull

# Reset notifications if the computer is charging/discharging
if [ $BATTERY_DISCHARGING -eq 1 ] && [ -f $FULL_FILE ]; then
    rm $FULL_FILE
fi

# If the battery is charging and is full (and has not shown notification yet)
if [ $BATTERY_LEVEL -gt 95 ] && [ $BATTERY_DISCHARGING -eq 0 ] && [ ! -f $FULL_FILE ]; then
    touch $FULL_FILE
    dunstify -i "/usr/share/icons/Adwaita/32x32/legacy/battery-full-charged-symbolic.symbolic.png" "Battery Charged" "Battery is fully charged." -r 9994
    /usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 0.78
    /usr/bin/pw-play ~/.window_manager_files_and_scripts/system_script/battery_full.wav > /dev/null 2>&1
    /usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ $var &
# If the battery is low and is not charging (and has not shown notification yet)
elif [ $BATTERY_LEVEL -le $WARNING_LEVEL ] && [ $BATTERY_DISCHARGING -eq 1 ]; then
    dunstify -i "/usr/share/icons/Adwaita/32x32/legacy/battery-caution.png" "Low Battery" "${BATTERY_LEVEL}% of battery remaining." -u critical -r 9994
#    touch $EMPTY_FILE
    /usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 0.45
    /usr/bin/pw-play ~/.window_manager_files_and_scripts/system_script/09_Tweeters.ogg > /dev/null 2>&1
    /usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ $var &
fi

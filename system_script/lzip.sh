#!/bin/bash

function zip_list {
  if [ -f "$*" ] ; then
    case "$*" in
      *.tar.bz2)   tar jtvf "$*" | less -iI  ;;
      *.tar.gz)    tar -tvf "$*" | less -iI  ;;
      *.bz2)       bunzip2 "$*"   ;;
      *.rar)       unrar l "$*" | less -iI  ;;
      *.gz)        gunzip "$*"    ;;
      *.tar)       tar tvf "$*" | less -iI  ;;
      *.tbz2)      tar tvf "$*" | less -iI  ;; # take some time
      *.tgz)       tar tvf "$*" | less -iI  ;; # take a second
      *.zip)       unzip -l "$*" | less -iI  ;;
      *.Z)         uncompress "$*";;
      *.7z)        7z l "$*" | less -iI  ;;
      *.7z.001)    7z l "$*" | less -iI  ;;
      *.deb)       ar tvf "$*" | less -iI  ;;
      *.tar.xz)    tar -tvf "$*" | less -iI  ;;
      *.tar.zst)   tar --use-compress-program=zstd -tvf "$*" | less -iI  ;;
      *.ova)       tar tvf "$*" | less -iI   ;;   # Not type of regular zip file but act like zip file & used as Virtual Box bundle file.
      *)           echo ""$*" cannot be extracted via lzip.sh" ;;
    esac
  else
    echo ""$*" is not a valid file"
  fi
}

zip_list $*

#!/bin/bash

function set_audio_level {
	arg=$1
	if [[ $arg = "up" ]]; then
		amixer -q set Master 5%+ && dunstify --icon="~/.window_manager_files_and_scripts/system_script/volume.png" "Volume Increase" -r 9991 "$(amixer get Master | awk -F'[][]' 'END{print $2}')" -t 2000
		/usr/bin/pw-play /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga > /dev/null 2>&1
	elif [[ $arg = "down" ]]; then
		amixer -q set Master 5%- && dunstify --icon="~/.window_manager_files_and_scripts/system_script/volume.png" "Volume Decrease" -r 9991 "$(amixer get Master | awk -F'[][]' 'END{print $2}')" -t 2000
		/usr/bin/pw-play /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga > /dev/null 2>&1
	elif [[ $arg = "mute" ]]; then
		amixer -q set Master toggle
		#pactl set-sink-mute 0 toggle

		volm="$(amixer get Master|awk -F'[][]' 'END{print $6}')"

		if [ $volm = "off" ]
		then
    		dunstify --icon="~/.window_manager_files_and_scripts/system_script/mute.png" "Volume Mute" -r 9991 -t 2000
		else
    		dunstify --icon="~/.window_manager_files_and_scripts/system_script/volume.png" "Volume Unmute" -r 9991 -t 2000
		/usr/bin/pw-play /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga > /dev/null 2>&1
		fi
	fi
}

set_audio_level $1

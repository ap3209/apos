#!/bin/bash

function powermenu {
    options="Cancel\nShutdown\nRestart\nSleep\nLock"
    selected=$(echo -e $options | rofi -dmenu)
    if [[ $selected = "Shutdown" ]]; then
        systemctl poweroff
    elif [[ $selected = "Restart" ]]; then
        systemctl reboot
    elif [[ $selected = "Sleep" ]]; then
        systemctl suspend
    elif [[ $selected = "Lock" ]]; then
        slock
    elif [[ $selected = "Cancel" ]]; then
        return
    fi
}

powermenu

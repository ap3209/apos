#!/bin/bash

echo 'Which Headset?'
echo '1) ZEB-SOUND BOMB 1'
echo '2) realme Buds'
read ans

if [[ $ans = "1" ]]; then
	bluetoothctl power on
	bluetoothctl agent on
	bluetoothctl default-agent
	bluetoothctl connect 41:42:84:BE:96:B1
	bluetoothctl info 41:42:84:BE:96:B1 | awk '/Connected/ {print $2}'	
elif [[ $ans = "2" ]]; then
	bluetoothctl power on
        bluetoothctl agent on
        bluetoothctl default-agent
        bluetoothctl connect 26:BD:E1:EC:AE:D7 
        bluetoothctl info 26:BD:E1:EC:AE:D7 | awk '/Connected/ {print $2}'
fi

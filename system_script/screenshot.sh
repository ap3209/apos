#!/bin/bash

var="$(wpctl get-volume @DEFAULT_AUDIO_SINK@|awk -F' ' 'END{print $2}')"

function screenshot_type {
	arg=$1
	if [[ $arg = "full" ]]; then
		scrot -p ${HOME}/Pictures/screenshot/screenshot_"$(date '+%Y-%m-%d_%H-%M-%S')".png && dunstify --icon="~/.window_manager_files_and_scripts/system_script/screenshot.png" "ScreenShot is Saved" -r 9992 -t 2000
		/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 0.60
	        /usr/bin/pw-play /usr/share/sounds/freedesktop/stereo/camera-shutter.oga > /dev/null 2>&1
	        /usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ $var &
	elif [[ $arg = "ws" ]]; then
		scrot -s ${HOME}/Pictures/screenshot/screenshot_"$(date '+%Y-%m-%d_%H-%M-%S')".png && dunstify --icon="~/.window_manager_files_and_scripts/system_script/screenshot.png" "ScreenShot is Saved" -r 9992 -t 2000
		/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 0.60
	        /usr/bin/pw-play /usr/share/sounds/freedesktop/stereo/camera-shutter.oga > /dev/null 2>&1
		/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ $var &
	fi
}

screenshot_type $1

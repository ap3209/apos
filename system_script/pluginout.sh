#!/bin/bash

export DISPLAY=:0
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus"

var="$(wpctl get-volume @DEFAULT_AUDIO_SINK@|awk -F' ' 'END{print $2}')"

function pluginout {
	arg=$1
	if [[ $arg = "1" ]]; then
		/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 0.60
		/usr/bin/pw-play ~/.window_manager_files_and_scripts/system_script/charger-connected.wav > /dev/null 2>&1
		/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ $var &
		pkill -f dwm_statusbar
		~/.window_manager_files_and_scripts/system_script/dwm_statusbar &
	elif [[ $arg = "0" ]]; then
		/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 0.60
		/usr/bin/pw-play ~/.window_manager_files_and_scripts/system_script/charger-removed.wav > /dev/null 2>&1
		/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ $var &
		pkill -f dwm_statusbar
		~/.window_manager_files_and_scripts/system_script/dwm_statusbar &
	fi
}

pluginout $1

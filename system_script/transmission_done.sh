#!/bin/sh

var="$(wpctl get-volume @DEFAULT_AUDIO_SINK@|awk -F' ' 'END{print $2}')"

notify-send "Transmission-daemon" "⬇️ $TR_TORRENT_NAME has completely downloaded."
/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 0.60
/usr/bin/pw-play /usr/share/sounds/freedesktop/stereo/complete.oga > /dev/null 2>&1
/usr/bin/wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ $var &

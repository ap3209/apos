#!/bin/bash

# Default directory for qemu disk & need to setup manauly.
qemuDir='/mnt/LinuxStorage/qemu-Kvm_disk' 

if [ -z "$qemuDir" ]; then
    echo "This script required to set default path of qemu directory"
    exit 1
fi

echo 'Choose the following disk option.'
echo '1) Create a dynamic virtual disk.'
echo '2) Convert pre-allocate full size disk into dynamic disk(Note:- This option is not tested deeply may problem occurs).' 
echo '3) Convert vdi disk file to qcow2 disk file.'
echo '4) Convert vmdk disk file to qcow2 disk file(Some time this file is bundal in ova,For more info about OVA file select option 5).'
echo '5) More details for OVA File & other images disk are found in this page.'
echo '6) Quit'

echo 'select number:'
read ans

if [[ $ans = "1" ]]; then
	
	clear
	echo -e "\nThis will Create a dynamic virtual disk."
#	echo "Where: disk-size is the maximum disk size (you can use k, M, G, T, P or E as suffixes)."
	echo -e "\nPlease enter disk name:"
	read dname
	echo -e "\nPlease enter disk-size (use k, M, G, T, P or E as suffixes to specify size of disk):" 
	read dsize
	echo -e "\n" && qemu-img create -f qcow2 -o preallocation=off $qemuDir/$dname $dsize
	echo -e "\nCreated $dname disk, Enjoy your virtual machine."

elif [[ $ans = "2" ]]; then

# where:
#
# the -f format flag specifies the format of the input disk
# the -O format flag specifies the format of the output disk
# the -o flag is used to specify some options for the output file, such as the way data is allocated (in this case, no data preallocation)
	
	clear
	echo -e "\nThis will Convert pre-allocate full size disk into dynamic disk(that uses preallocation=off)."
        echo -e "\nPlease enter disk name with full path(Avoid '~/' which causes an error):"
	read dname
	newdname=$(echo $dname |awk -F / '{gsub(/\.qcow2$/, "", $NF); print $NF}')1.qcow2
	echo -e "\nThe file is saving on $qemuDir/$newdname"
	qemu-img convert -f qcow2 -O qcow2 -o preallocation=off  $dname $qemuDir/$newdname
	echo -e "\nConverted pre-allocate full size disk into dynamic disk, Enjoy your virtual machine."	


elif [[ $ans = "3" ]]; then

# Avoid '~/' which causes an error on the script; '~/' works on direct command.

	clear
	echo -e "\nThis will Convert vdi disk file to qcow2 disk file"
	echo -e "\nPlease enter disk name with full path(Avoid '~/' which causes an error):"
	read dname
	newdname=$(echo $dname |awk -F / '{gsub(/\.vdi$/, "", $NF); print $NF}').qcow2
	echo -e "\nThe file is saving on $qemuDir/$newdname"
	qemu-img convert -f vdi -O qcow2 $dname $qemuDir/$newdname
	echo -e "\nConverted vdi to qcow2 disk, Enjoy your virtual machine."	

elif [[ $ans = "4" ]]; then

	clear
	echo -e "\nThis will Convert vmdk disk file to qcow2 disk file"
	echo -e "\nPlease enter disk name with full path(Avoid '~/' which causes an error):"
	read dname
	newdname=$(echo $dname |awk -F / '{gsub(/\.vmdk$/, "", $NF); print $NF}').qcow2
	echo -e "\nThe file is saving on $qemuDir/$newdname"
	qemu-img convert $dname $qemuDir/$newdname -O qcow2
	echo -e "\nConverted vmdk to qcow2 disk, Enjoy your virtual machine."	

elif [[ $ans = "5" ]]; then

clear

echo -e '	How To Convert OVA Image to QCOW2 (QEMU and virt-manager)'

echo -e '\nFirstly, navigate to the directory your .OVA file is stored in and open a terminal. Using the tar command, you need to extract its contents. Replace <image-name> with the name of your file, e.g. “linux_distro_amd64.ova”'

echo -e '\ntar -xvf <image-name>.ova'

echo -e	'\nWhen extracted, you’ll get a number of files but we’re only interested in the .VMDK file. Next, assuming you have QEMU / virt-manager already installed, we need to convert the VMDK to a QCOW2 image. Be patient as it will take a long time depending on the size of your image and the processing power of your computer. or to convert the VMDK to a QCOW2 image use option 4 on this script.'

echo -e '\nqemu-img convert <image-name>-disk001.vmdk <image-name>.qcow2 -O qcow2'

echo -e '\n==================================================================================================================================='

echo -e	'	Convert vdi to raw img'

echo -e '\nIn case you would like to first create raw uncompressed image of format .img, here is how to do it.'

echo -e '\nVBoxManage clonehd --format RAW <image-name>.vdi <image-name>.img'

echo -e '\nNOTE: When you do convert a .vdi to a .img, The format .img file is uncompressed vdi and it will be the maximum size that you set your .vdi to grow up to, maximum size it can grow up to.'

echo -e '\nConvert .img to qcow2'

echo -e '\nYou can as well convert .img to qcow2'

echo -e '\nqemu-img convert -f raw <image-name>.img -O qcow2 <image-name>.qcow2'

echo -e '\nIf you would like to .img back to .vdi, run'

echo -e '\nVBoxManage convertdd <image-name>.img <image-name>.vdi'

echo -e '\nThat’s all.You are done doing any of above processes'

echo -e '\n==================================================================================================================================='

echo -e	'	How to create preallocation disk(Mention as history info)'

echo -e '\nqemu-img create -f qcow2 -o preallocation=falloc <image-name>.qcow2 50G\n'

read -n 1 -r -p "Press any key to close..."

elif [[ $ans = "6" ]]; then

	exit 0

fi

#!/bin/sh

var0="$PWD"
mkdir ~/.window_manager_files_and_scripts
cd ~/.window_manager_files_and_scripts/
var1="$PWD"
usname="$(cat ~/username.txt)"
rm -rf ~/username.txt
cable="$(cat ~/cable.txt)"
rm -rf ~/cable.txt
wifi="$(cat ~/wifi.txt)"
rm -rf ~/wifi.txt

# clone core repo
git clone https://gitlab.com/ap3209/st.git

git clone https://gitlab.com/ap3209/dwm.git

git clone https://gitlab.com/ap3209/slock.git

git clone https://gitlab.com/ap3209/dotfiles.git

mv ~/.bashrc ~/.bashrc_this_system_original_file

cd "$var1/dotfiles" && cp -r .fonts .xsession .gtkrc-2.0 .nanorc .vimrc .vim .local .bashrc ~/

cd "$var1/dotfiles/.config" && mkdir -p ~/.config && cp -r dunst gtk-2.0 gtk-3.0 rofi picom kitty lf mpv transmission-daemon copyq mimeapps.list ~/.config/

cd "$var0" && cp -r 'system_script' "$var1" && cd "$var1/system_script" && chmod +x *.sh

mkdir -p ~/Pictures/screenshot ~/Downloads ~/Videos

mkdir -p ~/.portable_apps && git clone https://github.com/corpnewt/ProperTree.git ~/.portable_apps/ProperTree

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -P ~/Downloads/

# XDM latest pre release download
latest_pre_release_url=$(curl -s https://api.github.com/repos/subhra74/xdm/releases | jq -r '.[] | select(.prerelease == true) | .assets[2].browser_download_url' | awk '{for (i=1; i<=NF; i++) print $i}' | awk 'NR==1 {print; exit}') && wget $latest_pre_release_url -P ~/Downloads

# XDM latest release download
#cd ~/Downloads/ && curl -s https://api.github.com/repos/subhra74/xdm/releases/latest | grep "browser_download_url.*tar.xz" | cut -d : -f 2,3 | tr -d \" | wget -qi - && cd ..

wget https://github.com/notyz77/evo_cursor/releases/download/v0.1/evo_cursor.tar.gz -P ~/Downloads
tar -xf ~/Downloads/evo_cursor.tar.gz -C ~/Downloads

wget https://rvgl.org/downloads/install_rvgl.py -P ~/Downloads

git clone https://github.com/notyz77/wallpapers.git ~/Pictures/wallpapers

wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip -P ~/Downloads/ && unzip ~/Downloads/platform-tools-latest-linux.zip -d ~/.portable_apps/

# According to user input it will be modify the dwm_statusbar script
if [ "$wifi" = "nowifi" ]; then
    echo "This system do not have Wifi Adapter."
    sed -i '5d;8,9d' ~/.window_manager_files_and_scripts/system_script/dwm_statusbar
    sed -i "s/enp4s0/$cable/" ~/.window_manager_files_and_scripts/system_script/dwm_statusbar
elif [ "$wifi" != "nowifi" ]; then
    echo "This system have Wifi Adapter."
    sed -i "s/enp4s0/$cable/" ~/.window_manager_files_and_scripts/system_script/dwm_statusbar
    sed -i "s/enp4s0/$cable/" ~/.window_manager_files_and_scripts/dotfiles/dwm_statusbar.laptop
    sed -i "s/wlo1/$wifi/" ~/.window_manager_files_and_scripts/system_script/dwm_statusbar
    sed -i "s/wlo1/$wifi/" ~/.window_manager_files_and_scripts/dotfiles/dwm_statusbar.laptop
fi

# sed edit
sed -i "s/1usname1/$usname/" "$var1/dotfiles/lightdm/lightdm-gtk-greeter.conf-debian-11"
sed -i "s/1ap1/$usname/" "$var1/dwm/config.h"
sed -i "s/1ap1/$usname/" ~/.local/share/applications/ProperTree.desktop
sed -i "s/1ap1/$usname/" ~/.local/share/applications/lf.desktop
sed -i "s/1ap1/$usname/" ~/.config/lf/lfrc
sed -i "s/1ap1/$usname/" ~/.config/transmission-daemon/settings.json

# vs code extensions
code --install-extension dracula-theme.theme-dracula && code --install-extension ms-vscode.cpptools && code --install-extension vscjava.vscode-java-pack && code --install-extension ms-python.python

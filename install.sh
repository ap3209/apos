#!/bin/sh

if [ $(id -u) -ne 0 ]; then
        echo "Only root can do this"
        exit 1
fi

# asking username
echo "Type your username:"
read usname

# asking for network names
ls -lah /sys/class/net/
echo "Type your cable name it start with 'en' something something:"
read cable

ls -lah /sys/class/net/
echo "Type your wifi name it start with 'wl' something something (if don't have wifi type 'nowifi'):"
read wifi

# adding contrib non-free-firmware and backport repos
echo "enabling Contrib Non-free and Backport Repos"
cp /etc/apt/sources.list /etc/apt/sources.list.bak
codename1="$(cat /etc/os-release |grep 'VERSION_CODENAME' |awk -F'=' '{print $2}')"
sed -i 's/main.*/main contrib non-free-firmware non-free/gI' /etc/apt/sources.list
echo '' >> /etc/apt/sources.list && echo "deb http://deb.debian.org/debian/ $codename1-backports main contrib non-free-firmware non-free" >> /etc/apt/sources.list

# repository update
apt update

# system upgrade
apt dist-upgrade -y

# system packages and apps
apt install xorg build-essential libx11-dev libxinerama-dev libxft-dev libxrandr-dev nitrogen picom dunst libnotify-bin alsa-utils wireplumber pulsemixer htop pcmanfm lightdm lightdm-gtk-greeter numlockx psmisc fonts-noto fonts-noto-color-emoji elementary-xfce-icon-theme bash-completion scrot sxiv bluez libspa-0.2-bluetooth rofi virt-manager mpv firefox-esr copyq ufw curl wget apt-transport-https gpg python3-tk neofetch vim network-manager unzip unrar p7zip-full klavaro kodi kodi-pvr-iptvsimple kitty flatpak imagemagick ffmpegthumbnailer jq xdotool cmatrix transmission-daemon simple-scan -y

# Add the Flathub repository
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# revolt dependecy
apt install libsdl2-2.0-0 libsdl2-image-2.0-0 libenet7 libopenal1 -y

# installing some apt repository apps
curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|tee /etc/apt/sources.list.d/brave-browser-release.list

wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg

install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/

sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'

rm -f packages.microsoft.gpg

apt update

apt install brave-browser code -y

# Adding username.txt, cable.txt & wifi.txt, so non_superuser_insatll can access those variable
echo $usname > /home/$usname/username.txt
echo $cable > /home/$usname/cable.txt
echo $wifi > /home/$usname/wifi.txt

sudo -u $SUDO_USER bash -c '\
        ${HOME}/DWM_Debian_APOS/non_superuser_install.sh
'
echo
echo "*********************************************************"
echo "*			now i am again root		*"
echo "*********************************************************"
echo
var1="$PWD"
cd /home/$usname/.window_manager_files_and_scripts/
var2="$PWD"

echo $var1
echo $var2

cd "$var2/dwm" && make clean install
cd "$var2/st" && make clean install
cd "$var2/slock" && make clean install

echo
echo "*********************************************************"
echo "*			Lightdm configer		*"
echo "*********************************************************"
echo

cd /home/$usname/Pictures/wallpapers/ && cp 0031.jpeg 0026.png /etc/lightdm/
cd "$var2/dotfiles/lightdm" && cp lightdm.conf lightdm-gtk-greeter.conf /etc/lightdm/
mkdir -p /usr/share/xsessions
cd "$var1" && cp dwm.desktop /usr/share/xsessions

# virt-manager adding in libvirt group
usermod -a -G libvirt $usname

# dpkg install
dpkg -i /home/$usname/Downloads/google-chrome-stable_current_amd64.deb
apt install -f -y

# xdm install
dpkg -i /home/$usname/Downloads/xdman_gtk_*.deb
apt install -f -y

# evo cursor install
mv /home/$usname/Downloads/evo_cursor/ /usr/share/icons/
sed -i "s/Inherits/#Inherits/" /usr/share/icons/Adwaita/cursor.theme
echo 'Inherits=evo_cursor' >> /usr/share/icons/Adwaita/cursor.theme

# updating default terminal
update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/st 100

# network-manager configer
sed -i "s/false/true/" /etc/NetworkManager/NetworkManager.conf
service NetworkManager restart

# Disable transmission-daemon statup from systemD job because transmission-daemon will be open local user manauly
systemctl disable transmission-daemon.service

#!/bin/sh

if [ $(id -u) -ne 0 ]; then
        echo "Only root can do this"
        exit 1
fi

# Asking username
echo "Type your username:"
read usname

# Removeing laptop Script & Files
cd /home/$usname/.window_manager_files_and_scripts/system_script/ && rm -rf 09_Tweeters.ogg battery_full.wav battery_low.wav batterynotify.sh brightness.png charger-connected.wav charger-removed.wav pluginout.sh set_brightness.sh

# Removeing volume general script & use custom desktop script
cd /home/$usname/.window_manager_files_and_scripts/system_script/ && rm -rf set_volume.sh && mv set_volume.sh.desktop set_volume.sh

# Removeing picom laptop config
cd /home/$usname/.config/picom/ && rm -rf picom.conf.laptop

# Adding udev rule for disabling keyboad in sleep
cp /home/$usname/.window_manager_files_and_scripts/dotfiles/udev/50-wake-on-device.rules /etc/udev/rules.d/

echo 'Patch Finished'

#!/bin/sh

if [ $(id -u) -ne 0 ]; then
        echo "Only root can do this"
        exit 1
fi

# Asking username
echo "Type your username:"
read usname

# install file for laptop
apt install xbacklight tpl

# laptop brightness control package & fix file
cp /home/$usname/.window_manager_files_and_scripts/dotfiles/xorg_file/20-intel.conf /etc/X11/xorg.conf.d/

# synaptics touchpad setting
cp /home/$usname/.window_manager_files_and_scripts/dotfiles/xorg_file/synaptics.conf /etc/X11/xorg.conf.d/

# Removeing picom laptop config
cd /home/$usname/.config/picom/ && rm -rf picom.conf && mv picom.conf.laptop picom.conf

# Adding udev rule for charging (Connect/Discnnect) voice
cp /home/$usname/.window_manager_files_and_scripts/dotfiles/udev/23-PlugInOut.rules /etc/udev/rules.d/

# remove dwm_statusbar of desktop & replace dwm_statusbar.laptop
rm -rf /home/$usname/.window_manager_files_and_scripts/system_script/dwm_statusbar && cp /home/$usname/.window_manager_files_and_scripts/dotfiles/dwm_statusbar.laptop /home/$usname/.window_manager_files_and_scripts/system_script/dwm_statusbar

echo 'Patch Finished'
